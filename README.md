
# xyz Tile Layer Services List Belgium
**A list of xyz Tile Layer Services with data for Belgium**

The entire content of this repository is stored in this README.md document. All editing should be done on this file. The only other file is the LICENSE file.

More information about xyz Tile Layer Services kan be found on [Wikipedia](http://en.wikipedia.org/wiki/Tiled_web_map).



More detailed information about all listed services can be found on [xyz.michelstuyts.be](https://xyz.michelstuyts.be).

An xml file to import all these services into [QGIS](https://qgis.org) can be downloaded from https://xyz.michelstuyts.be/qgis.php?lang=en.

## Belgium

* **Map Belgium in 1873 - NGI**:   
https://wmts.ngi.be/arcgis/rest/services/seamless_carto__default__3857__140/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 7 - Maximum zoom level: 17

* **Map Belgium in 1904 - NGI**:   
https://wmts.ngi.be/arcgis/rest/services/seamless_carto__default__3857__450/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 7 - Maximum zoom level: 17

* **Map Belgium in 1939 - NGI**:   
https://wmts.ngi.be/arcgis/rest/services/seamless_carto__default__3857__800/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 7 - Maximum zoom level: 17

* **Map Belgium in 1969 - NGI**:   
https://wmts.ngi.be/arcgis/rest/services/seamless_carto__default__3857__1100/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 7 - Maximum zoom level: 17

* **Map Belgium in 1981 - NGI**:   
https://wmts.ngi.be/arcgis/rest/services/seamless_carto__default__3857__1220/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 7 - Maximum zoom level: 17

* **Map Belgium in 1989 - NGI**:   
https://wmts.ngi.be/arcgis/rest/services/seamless_carto__default__3857__1300/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 7 - Maximum zoom level: 17

* **NGI Map Belgium - BW - NGI**:   
https://cartoweb.wmts.ngi.be/1.0.0/topo/default_bw/3857/{z}/{y}/{x}.png  
Minimum zoom level: 7 - Maximum zoom level: 17

* **NGI Map Belgium - Default - NGI**:   
https://cartoweb.wmts.ngi.be/1.0.0/topo/default/3857/{z}/{y}/{x}.png  
Minimum zoom level: 7 - Maximum zoom level: 17

* **OpenStreetMap - Belgium (FR) - OpenStreetMap Belgium**:   
https://tile.osm.be/osmbe-fr/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 18

* **OpenStreetMap - Belgium (FR) - OpenStreetMap Belgium**:   
https://tile.openstreetmap.be/osmbe-fr/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 18

* **OpenStreetMap - Belgium (Multilingual) - OpenStreetMap Belgium**:   
https://tile.osm.be/osmbe/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 18

* **OpenStreetMap - Belgium (Multilingual) - OpenStreetMap Belgium**:   
https://tile.openstreetmap.be/osmbe/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 18

* **OpenStreetMap - Belgium (NL) - OpenStreetMap Belgium**:   
https://tile.osm.be/osmbe-nl/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 18

* **OpenStreetMap - Belgium (NL) - OpenStreetMap Belgium**:   
https://tile.openstreetmap.be/osmbe-nl/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 18

* **Ortho Belgium 1947-1954 - NGI**:   
https://wmts.ngi.be/arcgis/rest/services/ortho__default__3857__1947-1954/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 7 - Maximum zoom level: 17

* **Ortho Belgium 1969-1979 - NGI**:   
https://wmts.ngi.be/arcgis/rest/services/ortho__default__3857__1969-1979/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 7 - Maximum zoom level: 17

* **Ortho Belgium 1995 - NGI**:   
https://wmts.ngi.be/arcgis/rest/services/ortho__default__3857__1995/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 7 - Maximum zoom level: 17

* **Waze Mobile - Waze**:   
https://worldtiles1.waze.com/tiles/{z}/{x}/{y}.png - https://worldtiles2.waze.com/tiles/{z}/{x}/{y}.png - https://worldtiles3.waze.com/tiles/{z}/{x}/{y}.png - https://worldtiles4.waze.com/tiles/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 21



## Flanders

* **Atlas der Buurtwegen (ca 1840), Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/HISTCART/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=abw&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Digitaal Hoogtemodel Vlaanderen II, multidirectionale hillshade 0,25 m - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/DHMV/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=DHMV_II_HILL_25cm&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Digitaal Hoogtemodel Vlaanderen II, Skyview factor 0,25 m - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/DHMV/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=DHMV_II_SVF_25cm&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Ferrariskaart (1777), Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/HISTCART/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=ferraris&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Fricx-kaart (1744), Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/HISTCART/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=frickx&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **GRB Basiskaart (grijs) - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/GRB/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=grb_bsk_grijs&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **GRB Basiskaart (kleur) - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/GRB/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=grb_bsk&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **GRB Basiskaart (selectie) - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/GRB/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=grb_sel&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, grootschalig, winteropnamen, kleur, 2013-2015, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OGW/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=ogwrgb13_15vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, kleinschalig, zomeropnamen, kleur, 1979-1990, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OKZ/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=okzrgb79_90vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, kleinschalig, zomeropnamen, panchromatisch, 1971, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OKZ/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=okzpan71vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, winteropnamen, kleur, 2000-2003, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMW/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omwrgb00_03vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, winteropnamen, kleur, 2005-2007, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMW/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omwrgb05_07vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, winteropnamen, kleur, 2008-2011, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMW/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omwrgb08_11vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, winteropnamen, kleur, 2012, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMW/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omwrgb12vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, winteropnamen, kleur, 2013, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMW/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omwrgb13vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, winteropnamen, kleur, 2014, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMW/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omwrgb14vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, winteropnamen, kleur, 2015, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMW/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omwrgb15vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, winteropnamen, kleur, 2016, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMW/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omwrgb16vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, winteropnamen, kleur, 2017, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMW/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omwrgb17vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, winteropnamen, kleur, 2018, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMW/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omwrgb18vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, winteropnamen, kleur, 2019, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMW/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omwrgb19vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, winteropnamen, kleur, 2020, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMW/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omwrgb20vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, winteropnamen, kleur, 2021, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMW/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omwrgb21vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, winteropnamen, kleur, 2022, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMW/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omwrgb22vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, winteropnamen, kleur, meest recent, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMWRGBMRVL/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omwrgbmrvl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, zomeropnamen, grijswaarden, 2009, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMZ/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omzpan09vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, zomeropnamen, grijswaarden, 2012, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMZ/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omzpan12vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, zomeropnamen, grijswaarden, 2015, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMZ/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omzpan15vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, zomeropnamen, grijswaarden, 2018, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMZ/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omzpan18vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, zomeropnamen, kleur, 2009, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMZ/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omzrgb09vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, zomeropnamen, kleur, 2012, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMZ/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omzrgb12vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, zomeropnamen, kleur, 2015, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMZ/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omzrgb15vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, zomeropnamen, kleur, 2018, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMZ/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omzrgb18vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, zomeropnamen, kleur-infrarood, 2009, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMZ/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omznir09vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, zomeropnamen, kleur-infrarood, 2012, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMZ/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omznir12vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, zomeropnamen, kleur-infrarood, 2015, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMZ/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omznir15vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Orthofotomozaïek, middenschalig, zomeropnamen, kleur-infrarood, 2018, Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/OMZ/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=omznir18vl&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Popp-kaart, kopies (1842-1879), Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/HISTCART/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=popp&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21

* **Stadsplan Antwerpen - Stad Antwerpen**:   
https://tiles.arcgis.com/tiles/1KSVSmnHT2Lw9ea6/arcgis/rest/services/basemap_stadsplan_v5/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 0 - Maximum zoom level: 19

* **Topografische kaart Vandermaelen (1846-1854), Vlaanderen - Digitaal Vlaanderen**:   
https://geo.api.vlaanderen.be/HISTCART/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=vandermaelen&STYLE=&FORMAT=image/png&TILEMATRIXSET=GoogleMapsVL&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}  
Minimum zoom level: 0 - Maximum zoom level: 21



## World

* **2GIS - 2GIS**:   
http://tile2.maps.2gis.com/tiles?x={x}&y={y}&z={z}&v=1.1  
Minimum zoom level: 0 - Maximum zoom level: 8

* **AllRailMap - AllRailMap**:   
https://map.allrailmap.com/rail2/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 20

* **ALOS World 3D - 30m (Ver.2.1) - The Japan Aerospace Exploration Agency (JAXA)**:   
https://www.eorc.jaxa.jp/ALOS/aw3d30/MapTiles/v1804//{z}/{x}/{-y}.png  
Minimum zoom level: 0 - Maximum zoom level: 10

* **CARTO Base Antique - CARTO**:   
https://cartocdn_a.global.ssl.fastly.net/base-antique/{z}/{x}/{y}.png - https://cartocdn_b.global.ssl.fastly.net/base-antique/{z}/{x}/{y}.png - https://cartocdn_c.global.ssl.fastly.net/base-antique/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **CARTO Base Eco - CARTO**:   
https://cartocdn_a.global.ssl.fastly.net/base-eco/{z}/{x}/{y}.png - https://cartocdn_b.global.ssl.fastly.net/base-eco/{z}/{x}/{y}.png - https://cartocdn_c.global.ssl.fastly.net/base-eco/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **CARTO Base Flat Blue - CARTO**:   
https://cartocdn_a.global.ssl.fastly.net/base-flatblue/{z}/{x}/{y}.png - https://cartocdn_b.global.ssl.fastly.net/base-flatblue/{z}/{x}/{y}.png - https://cartocdn_c.global.ssl.fastly.net/base-flatblue/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **CARTO Base Midnight - CARTO**:   
https://cartocdn_a.global.ssl.fastly.net/base-midnight/{z}/{x}/{y}.png - https://cartocdn_b.global.ssl.fastly.net/base-midnight/{z}/{x}/{y}.png - https://cartocdn_c.global.ssl.fastly.net/base-midnight/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **CARTO Dark (All) - CARTO**:   
https://a.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png - https://b.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png - https://c.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **CARTO Dark (No Labels) - CARTO**:   
https://a.basemaps.cartocdn.com/dark_nolabels/{z}/{x}/{y}.png - https://b.basemaps.cartocdn.com/dark_nolabels/{z}/{x}/{y}.png - https://c.basemaps.cartocdn.com/dark_nolabels/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **CARTO Dark (Only labels) - CARTO**:   
https://a.basemaps.cartocdn.com/dark_only_labels/{z}/{x}/{y}.png - https://b.basemaps.cartocdn.com/dark_only_labels/{z}/{x}/{y}.png - https://c.basemaps.cartocdn.com/dark_only_labels/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **CARTO Light (All) - CARTO**:   
https://a.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png - https://b.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png - https://c.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **CARTO Light (No Labels) - CARTO**:   
https://a.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}.png - https://b.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}.png - https://c.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **CARTO Light (Only labels) - CARTO**:   
https://a.basemaps.cartocdn.com/light_only_labels/{z}/{x}/{y}.png - https://b.basemaps.cartocdn.com/light_only_labels/{z}/{x}/{y}.png - https://c.basemaps.cartocdn.com/light_only_labels/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **CARTO Voyager - CARTO**:   
https://a.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}.png - https://b.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}.png - https://c.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **CARTO Voyager (Labels Under) - CARTO**:   
https://a.basemaps.cartocdn.com/rastertiles/voyager_labels_under/{z}/{x}/{y}.png - https://b.basemaps.cartocdn.com/rastertiles/voyager_labels_under/{z}/{x}/{y}.png - https://c.basemaps.cartocdn.com/rastertiles/voyager_labels_under/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **CARTO Voyager (No Labels) - CARTO**:   
https://a.basemaps.cartocdn.com/rastertiles/voyager_nolabels/{z}/{x}/{y}.png - https://b.basemaps.cartocdn.com/rastertiles/voyager_nolabels/{z}/{x}/{y}.png - https://c.basemaps.cartocdn.com/rastertiles/voyager_nolabels/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **CARTO Voyager (Only labels) - CARTO**:   
https://a.basemaps.cartocdn.com/rastertiles/voyager_only_labels/{z}/{x}/{y}.png - https://b.basemaps.cartocdn.com/rastertiles/voyager_only_labels/{z}/{x}/{y}.png - https://c.basemaps.cartocdn.com/rastertiles/voyager_only_labels/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **CyclOSM - openstreetmap.fr**:   
https://a.tile-cyclosm.openstreetmap.fr/cyclosm/{z}/{x}/{y}.png - https://b.tile-cyclosm.openstreetmap.fr/cyclosm/{z}/{x}/{y}.png - https://c.tile-cyclosm.openstreetmap.fr/cyclosm/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 16

* **Esri National Geographic World Map - Esri**:   
https://server.arcgisonline.com/arcgis/rest/services/NatGeo_World_Map/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Esri World Boundaries and Places - Esri**:   
https://server.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Esri World Boundaries and Places (Alternate) - Esri**:   
https://server.arcgisonline.com/ArcGIS/rest/services/Reference/World_Boundaries_and_Places_Alternate/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Esri World Dark Gray Base Layer - Esri**:   
https://server.arcgisonline.com/arcgis/rest/services/Canvas/World_Dark_Gray_Base/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Esri World Dark Gray Reference Layer - Esri**:   
https://server.arcgisonline.com/arcgis/rest/services/Canvas/World_Dark_Gray_Reference/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Esri World Hillshade - Esri**:   
https://server.arcgisonline.com/arcgis/rest/services/Elevation/World_Hillshade/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Esri World Imagery - Esri**:   
https://server.arcgisonline.com/arcgis/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Esri World Light Gray Base Layer - Esri**:   
https://server.arcgisonline.com/arcgis/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Esri World Light Gray Reference Layer - Esri**:   
https://server.arcgisonline.com/arcgis/rest/services/Canvas/World_Light_Gray_Reference/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Esri World Navigation Charts - Esri**:   
https://server.arcgisonline.com/arcgis/rest/services/Specialty/World_Navigation_Charts/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Esri World Ocean Base - Esri**:   
https://server.arcgisonline.com/arcgis/rest/services/Ocean/World_Ocean_Base/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Esri World Ocean Reference - Esri**:   
https://server.arcgisonline.com/arcgis/rest/services/Ocean/World_Ocean_Reference/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Esri World Physical Map - Esri**:   
https://server.arcgisonline.com/arcgis/rest/services/World_Physical_Map/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Esri World Reference Overlay - Esri**:   
https://server.arcgisonline.com/arcgis/rest/services/Reference/World_Reference_Overlay/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Esri World Shaded Relief - Esri**:   
https://server.arcgisonline.com/arcgis/rest/services/World_Shaded_Relief/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Esri World Street Map - Esri**:   
https://server.arcgisonline.com/arcgis/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Esri World Terrain Base Map - Esri**:   
https://server.arcgisonline.com/arcgis/rest/services/World_Terrain_Base/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Esri World Topo Map - Esri**:   
https://server.arcgisonline.com/arcgis/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Esri World Transportation - Esri**:   
https://server.arcgisonline.com/arcgis/rest/services/Reference/World_Transportation/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 0 - Maximum zoom level: 24

* **F4map 2D - F4**:   
https://tile1.f4map.com/tiles/f4_2d/{z}/{x}/{y}.png - https://tile2.f4map.com/tiles/f4_2d/{z}/{x}/{y}.png - https://tile3.f4map.com/tiles/f4_2d/{z}/{x}/{y}.png - https://tile4.f4map.com/tiles/f4_2d/{z}/{x}/{y}.png  
Minimum zoom level: 1 - Maximum zoom level: 19

* **Google Bike - Google**:   
https://mt1.google.com/vt/lyrs=m,bike&x={x}&y={y}&z={z}  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Google Maps - Google**:   
https://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Google Maps Basic No Labels - Google**:   
https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i{z}!2i{x}!3i{y}!4i256!2m3!1e0!2sm!3i{y}!3m14!2snl!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy5lOmwudHxwLnY6b2ZmLHMudDo4MnxzLmU6Zy5mfHAuYzojZmZmNWY1ZjJ8cC52Om9uLHMudDoxfHAudjpvZmYscy50OjR8cC52Om9mZixzLnQ6Mzd8cC52Om9mZixzLnQ6ODF8cy5lOmcuZnxwLmM6I2ZmZmZmZmZmfHAudjpvbixzLnQ6MzN8cC52Om9mZixzLnQ6MzZ8cC52Om9mZixzLnQ6Mzh8cC52Om9mZixzLnQ6MzV8cC52Om9mZixzLnQ6Mzl8cC52Om9mZixzLnQ6NDl8cy5lOmd8cC5jOiNmZmZmZmZmZnxwLnY6c2ltcGxpZmllZCxzLnQ6NTB8cC52OnNpbXBsaWZpZWR8cC5jOiNmZmZmZmZmZixzLnQ6NDl8cy5lOmwuaXxwLmM6I2ZmZmZmZmZmfHAudjpvZmYscy50OjQ5fHMuZTpsLml8cC52Om9mZixzLnQ6NTB8cC5jOiNmZmZmZmZmZixzLnQ6NTF8cC5jOiNmZmZmZmZmZixzLnQ6NDB8cy5lOmwuaXxwLnY6b2ZmLHMudDoyfHMuZTpsLml8cC52Om9mZixzLnQ6NnxwLmM6I2ZmNzFjOGQ0LHMudDo1fHAuYzojZmZlNWU4ZTcscy50OjQwfHAuYzojZmY4YmExMjkscy50OjN8cC5jOiNmZmZmZmZmZixzLnQ6Mzl8cy5lOmd8cC5jOiNmZmM3YzdjN3xwLnY6b2ZmLHMudDo2fHAuYzojZmZhMGQzZDMscy50OjQwfHAuYzojZmY5MWI2NWQscy50OjQwfHAuZzoxLjUxLHMudDo1MXxwLnY6b2ZmLHMudDo1MXxzLmU6Z3xwLnY6b24scy50OjM0fHMuZTpnfHAudjpvZmYscy50OjV8cC52Om9mZixzLnQ6M3xzLmU6bHxwLnY6b2ZmLHMudDo1MHxzLmU6Z3xwLnY6c2ltcGxpZmllZCxzLnQ6NTF8cC52OnNpbXBsaWZpZWQscy50OjMscy50OjMscy50OjQ5!4e0!23i1301875  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Google Maps Black and White - Google**:   
https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i{z}!2i{x}!3i{y}!4i256!2m3!1e0!2sm!3i{y}!3m14!2snl!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjN8cy5lOmx8cC52Om9uLHMudDoyfHAudjpvZmYscy50OjF8cC52Om9mZixzLnQ6M3xzLmU6Zy5mfHAuYzojZmYwMDAwMDB8cC53OjEscy50OjN8cy5lOmcuc3xwLmM6I2ZmMDAwMDAwfHAudzowLjgscy50OjV8cC5jOiNmZmZmZmZmZixzLnQ6NnxwLnY6b2ZmLHMudDo0fHAudjpvZmYscy5lOmx8cC52Om9mZixzLmU6bC50fHAudjpvbixzLmU6bC50LnN8cC5jOiNmZmZmZmZmZixzLmU6bC50LmZ8cC5jOiNmZjAwMDAwMCxzLmU6bC5pfHAudjpvbg!4e0!23i1301875  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Google Maps Clean Grey - Google**:   
https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i{z}!2i{x}!3i{y}!4i256!2m3!1e0!2sm!3i{y}!3m14!2snl!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjF8cy5lOmx8cC52Om9mZixzLnQ6MTd8cy5lOmcuc3xwLnY6b2ZmLHMudDoxOHxzLmU6Zy5zfHAudjpvZmYscy50OjV8cy5lOmd8cC52Om9ufHAuYzojZmZlM2UzZTMscy50OjgyfHMuZTpsfHAudjpvZmYscy50OjJ8cC52Om9mZixzLnQ6M3xwLmM6I2ZmY2NjY2NjLHMudDozfHMuZTpsfHAudjpvZmYscy50OjR8cy5lOmwuaXxwLnY6b2ZmLHMudDo2NXxzLmU6Z3xwLnY6b2ZmLHMudDo2NXxzLmU6bC50fHAudjpvZmYscy50OjEwNTl8cy5lOmd8cC52Om9mZixzLnQ6MTA1OXxzLmU6bHxwLnY6b2ZmLHMudDo2fHMuZTpnfHAuYzojZmZGRkZGRkYscy50OjZ8cy5lOmx8cC52Om9mZg!4e0!23i1301875  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Google Maps Countries Only - Google**:   
https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i{z}!2i{x}!3i{y}!4i256!2m3!1e0!2sm!3i{y}!3m17!2snl!3sUS!5e18!12m4!1e68!2m2!1sset!2sRoadmap!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjF8cC52Om9mZixzLnQ6MTd8cC52Om9ufHAuaDojZmYwMDAwLHMudDoxN3xzLmU6Zy5mfHAudjpzaW1wbGlmaWVkfHAuYzojZmZmZWZlZmUscy50OjE3fHMuZTpnLnN8cC52Om9ufHAuYzojZmYwMDAwMDAscy50OjV8cC52Om9mZixzLnQ6MnxwLnY6b2ZmLHMudDozfHAudjpvZmYscy50OjN8cy5lOmd8cC52Om9mZixzLnQ6NnxzLmU6bHxwLnY6b2Zm!4e0  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Google Maps Light - Google**:   
https://mt1.google.com/vt/lyrs=r&x={x}&y={y}&z={z}  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Google Maps Nature - Google**:   
https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i{z}!2i{x}!3i{y}!4i256!2m3!1e0!2sm!3i{y}!3m14!2snl!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cC5oOiNGRkE4MDB8cC5nOjEscy50OjQ5fHAuaDojNTNGRjAwfHAuczotNzN8cC5sOjQwfHAuZzoxLHMudDo1MHxwLmg6I0ZCRkYwMHxwLmc6MSxzLnQ6NTF8cC5oOiMwMEZGRkR8cC5sOjMwfHAuZzoxLHMudDo2fHAuaDojMDBCRkZGfHAuczo2fHAubDo4fHAuZzoxLHMudDoyfHAuaDojNjc5NzE0fHAuczozMy40fHAubDotMjUuNHxwLmc6MQ!4e0!23i1301875  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Google Maps Neutral Blue - Google**:   
https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i{z}!2i{x}!3i{y}!4i256!2m3!1e0!2sm!3i{y}!3m14!2snl!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjZ8cy5lOmd8cC5jOiNmZjE5MzM0MSxzLnQ6NXxzLmU6Z3xwLmM6I2ZmMmM1YTcxLHMudDozfHMuZTpnfHAuYzojZmYyOTc2OGF8cC5sOi0zNyxzLnQ6MnxzLmU6Z3xwLmM6I2ZmNDA2ZDgwLHMudDo0fHMuZTpnfHAuYzojZmY0MDZkODAscy5lOmwudC5zfHAudjpvbnxwLmM6I2ZmM2U2MDZmfHAudzoyfHAuZzowLjg0LHMuZTpsLnQuZnxwLmM6I2ZmZmZmZmZmLHMudDoxfHMuZTpnfHAudzowLjZ8cC5jOiNmZjFhMzU0MSxzLmU6bC5pfHAudjpvZmYscy50OjQwfHMuZTpnfHAuYzojZmYyYzVhNzE!4e0!23i1301875  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Google Maps Paper - Google**:   
https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i{z}!2i{x}!3i{y}!4i256!2m3!1e0!2sm!3i{y}!3m14!2snl!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjF8cC52Om9mZixzLnQ6NXxwLnY6c2ltcGxpZmllZHxwLmg6IzAwNjZmZnxwLnM6NzR8cC5sOjEwMCxzLnQ6MnxwLnY6c2ltcGxpZmllZCxzLnQ6M3xwLnY6c2ltcGxpZmllZCxzLnQ6NDl8cC52Om9mZnxwLnc6MC42fHAuczotODV8cC5sOjYxLHMudDo0OXxzLmU6Z3xwLnY6b24scy50OjUwfHAudjpvZmYscy50OjUwfHMuZTpnfHAudjpvbixzLnQ6NTF8cC52Om9uLHMudDo0fHAudjpzaW1wbGlmaWVkLHMudDo2fHAudjpzaW1wbGlmaWVkfHAuYzojZmY1Zjk0ZmZ8cC5sOjI2fHAuZzo1Ljg2!4e0!23i1301875  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Google Maps Retro - Google**:   
https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i{z}!2i{x}!3i{y}!4i256!2m3!1e0!2sm!3i{y}!3m14!2snl!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjF8cC52Om9mZixzLnQ6MnxwLnY6c2ltcGxpZmllZCxzLnQ6M3xzLmU6bHxwLnY6c2ltcGxpZmllZCxzLnQ6NnxwLnY6c2ltcGxpZmllZCxzLnQ6NHxwLnY6c2ltcGxpZmllZCxzLnQ6NXxwLnY6c2ltcGxpZmllZCxzLnQ6NDl8cC52Om9mZixzLnQ6NTF8cC52Om9uLHMudDo0OXxzLmU6Z3xwLnY6b24scy50OjZ8cC5jOiNmZjg0YWZhM3xwLmw6NTIscC5zOi0xN3xwLmc6MC4zNixzLnQ6NjV8cy5lOmd8cC5jOiNmZjNmNTE4Yw!4e0!23i1301875  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Google Maps Shades of Grey - Google**:   
https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i{z}!2i{x}!3i{y}!4i256!2m3!1e0!2sm!3i{y}!3m14!2snl!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy5lOmwudC5mfHAuczozNnxwLmM6I2ZmMDAwMDAwfHAubDo0MCxzLmU6bC50LnN8cC52Om9ufHAuYzojZmYwMDAwMDB8cC5sOjE2LHMuZTpsLml8cC52Om9mZixzLnQ6MXxzLmU6Zy5mfHAuYzojZmYwMDAwMDB8cC5sOjIwLHMudDoxfHMuZTpnLnN8cC5jOiNmZjAwMDAwMHxwLmw6MTd8cC53OjEuMixzLnQ6NXxzLmU6Z3xwLmM6I2ZmMDAwMDAwfHAubDoyMCxzLnQ6MnxzLmU6Z3xwLmM6I2ZmMDAwMDAwfHAubDoyMSxzLnQ6NDl8cy5lOmcuZnxwLmM6I2ZmMDAwMDAwfHAubDoxNyxzLnQ6NDl8cy5lOmcuc3xwLmM6I2ZmMDAwMDAwfHAubDoyOXxwLnc6MC4yLHMudDo1MHxzLmU6Z3xwLmM6I2ZmMDAwMDAwfHAubDoxOCxzLnQ6NTF8cy5lOmd8cC5jOiNmZjAwMDAwMHxwLmw6MTYscy50OjR8cy5lOmd8cC5jOiNmZjAwMDAwMHxwLmw6MTkscy50OjZ8cy5lOmd8cC5jOiNmZjAwMDAwMHxwLmw6MTc!4e0!23i1301875  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Google Maps Subtle Grayscale - Google**:   
https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i{z}!2i{x}!3i{y}!4i256!2m3!1e0!2sm!3i{y}!3m14!2snl!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjF8cC5zOi0xMDAscy50OjE4fHAudjpvZmYscy50OjV8cC5zOi0xMDB8cC5sOjY1fHAudjpvbixzLnQ6MnxwLnM6LTEwMHxwLmw6NTB8cC52OnNpbXBsaWZpZWQscy50OjN8cC5zOi0xMDAscy50OjQ5fHAudjpzaW1wbGlmaWVkLHMudDo1MHxwLmw6MzAscy50OjUxfHAubDo0MCxzLnQ6NHxwLnM6LTEwMHxwLnY6c2ltcGxpZmllZCxzLnQ6NnxzLmU6Z3xwLmg6I2ZmZmYwMHxwLmw6LTI1fHAuczotOTcscy50OjZ8cy5lOmx8cC5sOi0yNXxwLnM6LTEwMA!4e0!23i1301875  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Google Roads - Google**:   
https://mt1.google.com/vt/lyrs=h&x={x}&y={y}&z={z}  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Google Satellite - Google**:   
https://mt1.google.com/vt/lyrs=s&x={x}&y={y}&z={z} - https://mt2.google.com/vt/lyrs=s&x={x}&y={y}&z={z} - https://mt3.google.com/vt/lyrs=s&x={x}&y={y}&z={z} - https://mt0.google.com/vt/lyrs=s&x={x}&y={y}&z={z}  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Google Satellite Hybrid - Google**:   
https://mt1.google.com/vt/lyrs=y&x={x}&y={y}&z={z}  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Google Terrain - Google**:   
https://mt1.google.com/vt/lyrs=t&x={x}&y={y}&z={z}  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Google Terrain Hybrid - Google**:   
https://mt1.google.com/vt/lyrs=p&x={x}&y={y}&z={z}  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Google Traffic - Google**:   
https://mt1.google.com/vt/lyrs=m,traffic&x={x}&y={y}&z={z}  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Google Transit - Google**:   
https://mt1.google.com/vt/lyrs=m,transit|vm:1&x={x}&y={y}&z={z}  
Minimum zoom level: 0 - Maximum zoom level: 22

* **HERE - Hybrid - HERE** **([API KEY NECESSARY!](https://developer.here.com))**:   
https://1.aerial.maps.api.here.com/maptile/2.1/maptile/newest/hybrid.day/{z}/{x}/{y}/256/png8 - https://2.aerial.maps.api.here.com/maptile/2.1/maptile/newest/hybrid.day/{z}/{x}/{y}/256/png8 - https://3.aerial.maps.api.here.com/maptile/2.1/maptile/newest/hybrid.day/{z}/{x}/{y}/256/png8 - https://4.aerial.maps.api.here.com/maptile/2.1/maptile/newest/hybrid.day/{z}/{x}/{y}/256/png8  
Minimum zoom level: 0 - Maximum zoom level: 20

* **HERE - Satellite - HERE** **([API KEY NECESSARY!](https://developer.here.com))**:   
https://1.aerial.maps.api.here.com/maptile/2.1/maptile/newest/satellite.day/{z}/{x}/{y}/256/png8 - https://2.aerial.maps.api.here.com/maptile/2.1/maptile/newest/satellite.day/{z}/{x}/{y}/256/png8 - https://3.aerial.maps.api.here.com/maptile/2.1/maptile/newest/satellite.day/{z}/{x}/{y}/256/png8 - https://4.aerial.maps.api.here.com/maptile/2.1/maptile/newest/satellite.day/{z}/{x}/{y}/256/png8  
Minimum zoom level: 0 - Maximum zoom level: 20

* **HERE - Terrain - HERE** **([API KEY NECESSARY!](https://developer.here.com))**:   
https://1.aerial.maps.api.here.com/maptile/2.1/maptile/newest/terrain.day/{z}/{x}/{y}/256/png8 - https://2.aerial.maps.api.here.com/maptile/2.1/maptile/newest/terrain.day/{z}/{x}/{y}/256/png8 - https://3.aerial.maps.api.here.com/maptile/2.1/maptile/newest/terrain.day/{z}/{x}/{y}/256/png8 - https://4.aerial.maps.api.here.com/maptile/2.1/maptile/newest/terrain.day/{z}/{x}/{y}/256/png8  
Minimum zoom level: 0 - Maximum zoom level: 20

* **iPhoto Slideshow Style - Apple**:   
http://gsp2.apple.com/tile?api=1&style=slideshow&layers=default&z={z}&x={x}&y={y}&v=9  
Minimum zoom level: 3 - Maximum zoom level: 14

* **Kartogiraffe - Base Map - Thomas Wendt**:   
https://www.kartogiraffe.de/tiles/tile.php?zoom={z}&x={x}&y={y}  
Minimum zoom level: 1 - Maximum zoom level: 24

* **Kartogiraffe - Bicycle Map - Thomas Wendt**:   
https://www.kartogiraffe.de/tiles/tile.php?zoom={z}&x={x}&y={y}&type=bicycle  
Minimum zoom level: 1 - Maximum zoom level: 24

* **Kartogiraffe - Hiking Map - Thomas Wendt**:   
https://www.kartogiraffe.de/tiles/tile.php?zoom={z}&x={x}&y={y}&type=hiking  
Minimum zoom level: 1 - Maximum zoom level: 24

* **Kartogiraffe - Transport Map - Thomas Wendt**:   
https://www.kartogiraffe.de/tiles/tile.php?zoom={z}&x={x}&y={y}&type=transport  
Minimum zoom level: 1 - Maximum zoom level: 24

* **Mapbox - Landsat Live Satellite - Mapbox** **([API KEY NECESSARY!](https://www.mapbox.com))**:   
https://a.tiles.mapbox.com/v4/mapbox.landsat-live/{z}/{x}/{y}@2x.jpg  
Minimum zoom level: 7 - Maximum zoom level: 12

* **Mapbox - Satellite - Mapbox** **([API KEY NECESSARY!](https://www.mapbox.com))**:   
https://a.tiles.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}@2x.jpg  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Maps.Refuges.Info - Hiking - MRI**:   
https://maps.refuges.info/hiking/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 18

* **Maptoolkit THillshading - Toursprung GmbH**:   
https://vtc-cdn.maptoolkit.net/hillshading/{z}/{x}/{y}.png  
Minimum zoom level: 1 - Maximum zoom level: 24

* **Mapzen Terrain (AWS) - Mapzen**:   
https://s3.amazonaws.com/elevation-tiles-prod/normal/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 15

* **Mapzen Terrarium (AWS) - Mapzen**:   
https://s3.amazonaws.com/elevation-tiles-prod/terrarium/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 15

* **MTB Map - Martin Tesař**:   
https://tile.mtbmap.cz/mtbmap_tiles/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 18

* **NASA GIBS MODIS Aqua Corrected Reflectance True Color - Nasa**:   
https://map1a.vis.earthdata.nasa.gov/wmts-webmerc/MODIS_Aqua_CorrectedReflectance_TrueColor/default/2018-11-08/GoogleMapsCompatible_Level9/{z}/{y}/{x}.jpg  
Minimum zoom level: 0 - Maximum zoom level: 9

* **NASA GIBS MODIS Terra Corrected Reflectance True Color - Nasa**:   
https://map1a.vis.earthdata.nasa.gov/wmts-webmerc/MODIS_Terra_CorrectedReflectance_TrueColor/default/2018-11-08/GoogleMapsCompatible_Level9/{z}/{y}/{x}.jpg  
Minimum zoom level: 0 - Maximum zoom level: 9

* **NASA GIBS VIIRS Earth At Night 2012 - Nasa**:   
https://map1.vis.earthdata.nasa.gov/wmts-webmerc/VIIRS_CityLights_2012/default//GoogleMapsCompatible_Level8/{z}/{y}/{x}.jpg  
Minimum zoom level: 1 - Maximum zoom level: 8

* **OpenCellid - Unwired Labs**:   
https://opencellid.org/images/maps/opencellid/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 10

* **OpenCycleMap - Thunderforest** **([API KEY NECESSARY!](https://www.thunderforest.com/))**:   
https://a.tile.thunderforest.com/cycle/{z}/{x}/{y}.png - https://b.tile.thunderforest.com/cycle/{z}/{x}/{y}.png - https://c.tile.thunderforest.com/cycle/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 22

* **OpenRailwayMap - OpenRailwayMap**:   
https://a.tiles.openrailwaymap.org/standard/{z}/{x}/{y}.png - https://b.tiles.openrailwaymap.org/standard/{z}/{x}/{y}.png - https://c.tiles.openrailwaymap.org/standard/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 19

* **OpenRailwayMap (Max Speed) - OpenRailwayMap**:   
https://a.tiles.openrailwaymap.org/maxspeed/{z}/{x}/{y}.png - https://b.tiles.openrailwaymap.org/maxspeed/{z}/{x}/{y}.png - https://c.tiles.openrailwaymap.org/maxspeed/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 19

* **OpenRailwayMap (Signals) - OpenRailwayMap**:   
https://a.tiles.openrailwaymap.org/signals/{z}/{x}/{y}.png - https://b.tiles.openrailwaymap.org/signals/{z}/{x}/{y}.png - https://c.tiles.openrailwaymap.org/signals/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 19

* **OpenRiverBoatMap - OpenStreetMap France**:   
https://a.tile.openstreetmap.fr/openriverboatmap/{z}/{x}/{y}.png - https://b.tile.openstreetmap.fr/openriverboatmap/{z}/{x}/{y}.png - https://c.tile.openstreetmap.fr/openriverboatmap/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 20

* **OpenSeaMap - OpenSeaMap**:   
https://tiles.openseamap.org/seamark/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **OpenSnowMap (Overlay) - OpenSnowMap**:   
https://www.opensnowmap.org/opensnowmap-overlay/{z}/{x}/{y}.png  
Minimum zoom level: 19 - Maximum zoom level: 20

* **OpenSnowMap (Pistes) - OpenSnowMap**:   
https://www.opensnowmap.org/tiles-pistes/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 20

* **OpenStreetMap - Skobbler**:   
https://tiles1-bc7b4da77e971c12cb0e069bffcf2771.skobblermaps.com/TileService/tiles/2.0/01021113210/7/{z}/{x}/{y}.png?traffic=false - https://tiles2-bc7b4da77e971c12cb0e069bffcf2771.skobblermaps.com/TileService/tiles/2.0/01021113210/7/{z}/{x}/{y}.png?traffic=false - https://tiles3-bc7b4da77e971c12cb0e069bffcf2771.skobblermaps.com/TileService/tiles/2.0/01021113210/7/{z}/{x}/{y}.png?traffic=false  
Minimum zoom level: 0 - Maximum zoom level: 18

* **OpenStreetMap ÖPNV-Karte - OpenStreetMap Deutschland**:   
https://tile.memomaps.de/tilegen/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 18

* **OpenStreetMap (with traffic) - Skobbler**:   
https://tiles1-bc7b4da77e971c12cb0e069bffcf2771.skobblermaps.com/TileService/tiles/2.0/01021113210/7/{z}/{x}/{y}.png?traffic=true - https://tiles2-bc7b4da77e971c12cb0e069bffcf2771.skobblermaps.com/TileService/tiles/2.0/01021113210/7/{z}/{x}/{y}.png?traffic=true - https://tiles3-bc7b4da77e971c12cb0e069bffcf2771.skobblermaps.com/TileService/tiles/2.0/01021113210/7/{z}/{x}/{y}.png?traffic=true  
Minimum zoom level: 0 - Maximum zoom level: 18

* **OpenStreetMap - Bergfex (512px) - bergfex gmbh**:   
https://tiles.bergfex.at/styles/bergfex-osm/{z}/{x}/{y}@2x.jpg  
Minimum zoom level: 0 - Maximum zoom level: 22

* **OpenStreetMap - Bergfex (Standard) - bergfex gmbh**:   
https://tiles.bergfex.at/styles/bergfex-osm/{z}/{x}/{y}.jpg  
Minimum zoom level: 0 - Maximum zoom level: 22

* **OpenStreetMap - Deutschland - OpenStreetMap - Deutschland**:   
https://tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 19

* **OpenStreetMap - France - OpenStreetMap France**:   
https://a.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png - https://b.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png - https://c.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 20

* **OpenStreetMap - HOT - Humanitarian OpenStreetMap Team (HOT)**:   
https://a.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png - https://b.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png - https://c.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 18

* **OpenStreetMap - LowZoom - RRZE**:   
https://osm.rrze.fau.de/lowzoom/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 19

* **OpenStreetMap - Maptoolkit Light - Toursprung GmbH**:   
https://vtile01.maptoolkit.net/styles/light/rendered/{z}/{x}/{y}.png - https://vtile02.maptoolkit.net/styles/light/rendered/{z}/{x}/{y}.png - https://vtile03.maptoolkit.net/styles/light/rendered/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 20

* **OpenStreetMap - Maptoolkit Terrain - Toursprung GmbH**:   
https://tile01.maptoolkit.net/terrain/{z}/{x}/{y}.png - https://tile02.maptoolkit.net/terrain/{z}/{x}/{y}.png - https://tile03.maptoolkit.net/terrain/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 18

* **OpenStreetMap - Maptoolkit Terrain New - Toursprung GmbH**:   
https://vtile01.maptoolkit.net/raster/toursprung/terrain/{z}/{x}/{y}.png - https://vtile02.maptoolkit.net/raster/toursprung/terrain/{z}/{x}/{y}.png - https://vtile03.maptoolkit.net/raster/toursprung/terrain/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 19

* **OpenStreetMap - Maptoolkit Terrain Winter - Toursprung GmbH**:   
https://vtile01.maptoolkit.net/raster/toursprung/terrainwinter/{z}/{x}/{y}.png - https://vtile02.maptoolkit.net/raster/toursprung/terrainwinter/{z}/{x}/{y}.png - https://vtile03.maptoolkit.net/raster/toursprung/terrainwinter/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 19

* **OpenStreetMap - Nop - OpenStreetMap Foundation**:   
https://www.wanderreitkarte.de/topo/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 19

* **OpenStreetMap - openstreetmap-carto - RRZE**:   
https://osm.rrze.fau.de/tiles/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 19

* **OpenStreetMap - openstreetmap-carto-de - RRZE**:   
https://osm.rrze.fau.de/osmde/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 19

* **OpenStreetMap - openstreetmap-carto-HD - RRZE**:   
https://a.osm.rrze.fau.de/osmhd/{z}/{x}/{y}.png - https://b.osm.rrze.fau.de/osmhd/{z}/{x}/{y}.png - https://c.osm.rrze.fau.de/osmhd/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 19

* **OpenStreetMap - OsmAnd - OsmAnd**:   
https://tile.osmand.net/hd/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 19

* **OpenStreetMap Carto Style (default) - OpenStreetMap Foundation**:   
https://tile.openstreetmap.org/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 19

* **OpenStreetMap GPS traces - OpenStreetMap Foundation**:   
https://gps-tile.openstreetmap.org/lines/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **OpenStreetMap HD - RRZE**:   
https://b.osm.rrze.fau.de/osmhd/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 19

* **OpenStreetMap WebMapp - WebMapp**:   
https://api.webmapp.it/tiles/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 16

* **OpenTopoMap - Friedrich-Alexander-Universität Erlangen-Nürnberg**:   
https://a.tile.opentopomap.org/{z}/{x}/{y}.png - https://b.tile.opentopomap.org/{z}/{x}/{y}.png - https://c.tile.opentopomap.org/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 16

* **OpenWeatherMap - Clouds New - OpenWeatherMap** **([API KEY NECESSARY!](https://openweathermap.org))**:   
http://tile.openweathermap.org/map/clouds_new/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **OpenWeatherMap - Precipitation - OpenWeatherMap** **([API KEY NECESSARY!](https://openweathermap.org))**:   
http://tile.openweathermap.org/map/precipitation_new/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **OpenWeatherMap - Sea level pressure - OpenWeatherMap** **([API KEY NECESSARY!](https://openweathermap.org))**:   
http://tile.openweathermap.org/map/pressure/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **OpenWeatherMap - Temperature - OpenWeatherMap** **([API KEY NECESSARY!](https://openweathermap.org))**:   
http://tile.openweathermap.org/map/temp_new/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **OpenWeatherMap - Wind speed - OpenWeatherMap** **([API KEY NECESSARY!](https://openweathermap.org))**:   
http://tile.openweathermap.org/map/wind/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **OpenWeatherMap - Wind speed New - OpenWeatherMap** **([API KEY NECESSARY!](https://openweathermap.org))**:   
http://tile.openweathermap.org/map/wind_new/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **osmapa-topo - osmapa**:   
https://a.tile.openstreetmap.pl/osmapa.pl/{z}/{x}/{y}.png - https://b.tile.openstreetmap.pl/osmapa.pl/{z}/{x}/{y}.png - https://c.tile.openstreetmap.pl/osmapa.pl/{z}/{x}/{y}.png - https://d.tile.openstreetmap.pl/osmapa.pl/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 17

* **SafeCast - SafeCast**:   
https://s3.amazonaws.com/te512.safecast.org/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 15

* **Seekarte - FreieTonne**:   
https://www.freietonne.de/seekarte/tah.openstreetmap.org/Tiles/TileCache.php?z={z}&x={x}&y={y}.png  
Minimum zoom level: 0 - Maximum zoom level: 18

* **Stamen Terrain - Stadiamaps** **([API KEY NECESSARY!]())**:   
https://tiles.stadiamaps.com/tiles/stamen_terrain/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Stamen Terrain Background - Stadiamaps** **([API KEY NECESSARY!]())**:   
https://tiles.stadiamaps.com/tiles/stamen_terrain_background/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Stamen Terrain Labels - Stadiamaps** **([API KEY NECESSARY!]())**:   
https://tiles.stadiamaps.com/tiles/stamen_terrain_labels/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Stamen Toner - Stadiamaps** **([API KEY NECESSARY!]())**:   
https://tiles.stadiamaps.com/tiles/stamen_toner/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Stamen Toner Background - Stadiamaps** **([API KEY NECESSARY!]())**:   
https://tiles.stadiamaps.com/tiles/stamen_toner_background/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Stamen Toner Labels - Stadiamaps** **([API KEY NECESSARY!]())**:   
https://tiles.stadiamaps.com/tiles/stamen_toner_labels/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Stamen Toner Lite - Stadiamaps** **([API KEY NECESSARY!]())**:   
https://tiles.stadiamaps.com/tiles/stamen_toner_lite/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Stamen Watercolor - Stadiamaps** **([API KEY NECESSARY!]())**:   
https://tiles.stadiamaps.com/tiles/stamen_watercolor/{z}/{x}/{y}.jpg  
Minimum zoom level: 0 - Maximum zoom level: 24

* **Thunderforest Landscape Map - Thunderforest** **([API KEY NECESSARY!](https://www.thunderforest.com/))**:   
https://a.tile.thunderforest.com/landscape/{z}/{x}/{y}.png - https://b.tile.thunderforest.com/landscape/{z}/{x}/{y}.png - https://c.tile.thunderforest.com/landscape/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Thunderforest Mobile Atlas - Thunderforest** **([API KEY NECESSARY!](https://www.thunderforest.com/))**:   
https://a.tile.thunderforest.com/mobile-atlas/{z}/{x}/{y}.png - https://b.tile.thunderforest.com/mobile-atlas/{z}/{x}/{y}.png - https://c.tile.thunderforest.com/mobile-atlas/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Thunderforest Neighbourhood Map - Thunderforest** **([API KEY NECESSARY!](https://www.thunderforest.com/))**:   
https://a.tile.thunderforest.com/neighbourhood/{z}/{x}/{y}.png - https://b.tile.thunderforest.com/neighbourhood/{z}/{x}/{y}.png - https://c.tile.thunderforest.com/neighbourhood/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Thunderforest Outdoors Map - Thunderforest** **([API KEY NECESSARY!](https://www.thunderforest.com/))**:   
https://a.tile.thunderforest.com/outdoors/{z}/{x}/{y}.png - https://b.tile.thunderforest.com/outdoors/{z}/{x}/{y}.png - https://c.tile.thunderforest.com/outdoors/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Thunderforest Pioneer Map - Thunderforest** **([API KEY NECESSARY!](https://www.thunderforest.com/))**:   
https://a.tile.thunderforest.com/pioneer/{z}/{x}/{y}.png - https://b.tile.thunderforest.com/pioneer/{z}/{x}/{y}.png - https://c.tile.thunderforest.com/pioneer/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Thunderforest Spinal Map - Thunderforest** **([API KEY NECESSARY!](https://www.thunderforest.com/))**:   
https://a.tile.thunderforest.com/spinal-map/{z}/{x}/{y}.png - https://b.tile.thunderforest.com/spinal-map/{z}/{x}/{y}.png - https://c.tile.thunderforest.com/spinal-map/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Thunderforest Transport Dark Map - Thunderforest** **([API KEY NECESSARY!](https://www.thunderforest.com/))**:   
https://a.tile.thunderforest.com/transport-dark/{z}/{x}/{y}.png - https://b.tile.thunderforest.com/transport-dark/{z}/{x}/{y}.png - https://c.tile.thunderforest.com/transport-dark/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Thunderforest Transport Map - Thunderforest** **([API KEY NECESSARY!](https://www.thunderforest.com/))**:   
https://a.tile.thunderforest.com/transport/{z}/{x}/{y}.png - https://b.tile.thunderforest.com/transport/{z}/{x}/{y}.png - https://c.tile.thunderforest.com/transport/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 22

* **Tracestrack Topo - Tracestrack** **([API KEY NECESSARY!](https://www.tracestrack.com))**:   
https://tile.tracestrack.com/topo__/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 14

* **ViaMichelin - Michelin**:   
https://map1.viamichelin.com/map/mapdirect?map=viamichelin&z={z}&x={x}&y={y}&format=png&version=202301111200&layer=background&protocol=https - https://map2.viamichelin.com/map/mapdirect?map=viamichelin&z={z}&x={x}&y={y}&format=png&version=202301111200&layer=background&protocol=https - https://map3.viamichelin.com/map/mapdirect?map=viamichelin&z={z}&x={x}&y={y}&format=png&version=202301111200&layer=background&protocol=https - https://map4.viamichelin.com/map/mapdirect?map=viamichelin&z={z}&x={x}&y={y}&format=png&version=202301111200&layer=background&protocol=https - https://map5.viamichelin.com/map/mapdirect?map=viamichelin&z={z}&x={x}&y={y}&format=png&version=202301111200&layer=background&protocol=https  
Minimum zoom level: 1 - Maximum zoom level: 19

* **ViaMichelin - Michelin**:   
https://staticmaps.webmichelin.com/michelin/202111190903/{z}/{x}/{y}.png  
Minimum zoom level: 2 - Maximum zoom level: 14

* **ViaMichelin Light - Michelin**:   
https://map1.viamichelin.com/map/mapdirect?map=light&z={z}&x={x}&y={y}&format=png&version=201503191157&layer=background - https://map2.viamichelin.com/map/mapdirect?map=light&z={z}&x={x}&y={y}&format=png&version=201503191157&layer=background - https://map3.viamichelin.com/map/mapdirect?map=light&z={z}&x={x}&y={y}&format=png&version=201503191157&layer=background - https://map4.viamichelin.com/map/mapdirect?map=light&z={z}&x={x}&y={y}&format=png&version=201503191157&layer=background - https://map5.viamichelin.com/map/mapdirect?map=light&z={z}&x={x}&y={y}&format=png&version=201503191157&layer=background  
Minimum zoom level: 1 - Maximum zoom level: 19

* **ViaMichelin Minimal - Michelin**:   
https://map1.viamichelin.com/map/mapdirect?map=hybrid&z={z}&x={x}&y={y}&format=png&version=201503191157&layer=network - https://map2.viamichelin.com/map/mapdirect?map=hybrid&z={z}&x={x}&y={y}&format=png&version=201503191157&layer=network - https://map3.viamichelin.com/map/mapdirect?map=hybrid&z={z}&x={x}&y={y}&format=png&version=201503191157&layer=network - https://map4.viamichelin.com/map/mapdirect?map=hybrid&z={z}&x={x}&y={y}&format=png&version=201503191157&layer=network - https://map5.viamichelin.com/map/mapdirect?map=hybrid&z={z}&x={x}&y={y}&format=png&version=201503191157&layer=network  
Minimum zoom level: 1 - Maximum zoom level: 19

* **Waymarkedtrails - Cycling - Waymarkedtrails**:   
https://tile.waymarkedtrails.org/cycling/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 18

* **Waymarkedtrails - Hiking - Waymarkedtrails**:   
https://tile.waymarkedtrails.org/hiking/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 18

* **Waymarkedtrails - MTB - Waymarkedtrails**:   
https://tile.waymarkedtrails.org/mtb/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 18

* **Waymarkedtrails - Riding - Waymarkedtrails**:   
https://tile.waymarkedtrails.org/riding/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 18

* **Waymarkedtrails - Skating - Waymarkedtrails**:   
https://tile.waymarkedtrails.org/skating/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 18

* **Waymarkedtrails - Slopes - Waymarkedtrails**:   
https://tile.waymarkedtrails.org/slopes/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 18

* **WhereGroup OSM Demo - WhereGroup**:   
https://osm-demo-a.wheregroup.com/tiles/1.0.0/osm/webmercator/{z}/{x}/{y}.png - https://osm-demo-b.wheregroup.com/tiles/1.0.0/osm/webmercator/{z}/{x}/{y}.png - https://osm-demo-c.wheregroup.com/tiles/1.0.0/osm/webmercator/{z}/{x}/{y}.png  
Minimum zoom level: 0 - Maximum zoom level: 22

* **World Imagery (Firefly) - Esri**:   
https://fly.maptiles.arcgis.com/arcgis/rest/services/World_Imagery_Firefly/MapServer/tile/{z}/{y}/{x}  
Minimum zoom level: 0 - Maximum zoom level: 19

* **Yandex Overlay Map - Yandex**:   
https://core-renderer-tiles.maps.yandex.net/tiles?l=skl&v=22.05.12-1-b220426173400&x={x}&y={y}&z={z}&scale=1&lang=ru_RU  
Minimum zoom level: 0 - Maximum zoom level: 23

* **Yandex Satellite - Yandex**:   
https://sat01.maps.yandex.net/tiles?l=sat&x={x}&y={y}&z={z} - https://sat02.maps.yandex.net/tiles?l=sat&x={x}&y={y}&z={z} - https://sat03.maps.yandex.net/tiles?l=sat&x={x}&y={y}&z={z} - https://sat04.maps.yandex.net/tiles?l=sat&x={x}&y={y}&z={z}  
Minimum zoom level: 0 - Maximum zoom level: 19

